Twitter-like Application
Create By: Leandro Henrique

Heads up: this is an Android project wrote in Java.
This project was created on Android Studio. If you are having problem to run Junit tests, look this link (http://goo.gl/JDTIYJ)

***** Classes: *****

- Message: a generic message used as model for more especialized classes

- TwitterMessage: a specialized class of Message which contains a text message and a list of topics, URIs and mentions

- GenericParser: a parser that receive a String and one or more Regex and return a list of found matches

- Regex: a class that stores constant Strings which represent Regexes used in the application


***** How it works *****

- The TwitterMessage class utilizes the GenericParser to process its own text message in order to indentify mentions, uris and topics.
- The GenericParser is a utility that can be used for multiple purposes in the application. It will always return a list of words (Strings) according to the regex(es) passed as argument.


***** Classes Directory *****

app.src.main.java.com.leandro.twitterengine.util
- GenericParser

app.src.main.java.com.leandro.twitterengine
- Message
- TwitterMessage
- Regexes


***** Unit Tests Directory *****

app.src.test.java.com.leandro.twitterengine.util
- GenericParserTest

app.src.test.java.com.leandro.twitterengine
- MessageTest
- TwitterMessageTest