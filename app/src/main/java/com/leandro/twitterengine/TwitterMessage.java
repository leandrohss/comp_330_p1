package com.leandro.twitterengine;

import static com.leandro.twitterengine.Regexes.*;

import com.leandro.twitterengine.util.GenericParser;

import java.util.ArrayList;
import java.util.List;

/**
 * A specialized message which holds twitter attributes (mentions, topics, URIs)
 *
 * Created by Leandro on 1/30/2016.
 */
public class TwitterMessage extends Message {

    private String message;

    private List<String> mentions = new ArrayList<>();
    private List<String> topics   = new ArrayList<>();
    private List<String> uris     = new ArrayList<>();

    public TwitterMessage(String message) {
        super(message);
        this.message = message;
        processMessage();
    }

    /*
     * Process the current message in order to achieve: mentions, topics and URIs
     */
    private void processMessage() {

        List<List<String>> results;
        results = GenericParser.parse(message, MENTIONS, TOPICS, URIS);

        // populate attributes
        for (int i = 0; i < results.size(); i++) {
            for (int j = 0; j < results.get(i).size(); j++) {

                String word = results.get(i).get(j);

                // clean the words
                while (word.endsWith(".") || word.endsWith(","))
                    word = word.substring(0, word.length() -1);

                if (i == 0)
                    mentions.add(word);
                else if (i == 1)
                    topics.add(word);
                else if (i == 2)
                    uris.add(word);
            }
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        processMessage();
    }

    public List<String> getMentions() {
        return mentions;
    }

    public List<String> getTopics() {
        return topics;
    }

    public List<String> getUris() {
        return uris;
    }
}
