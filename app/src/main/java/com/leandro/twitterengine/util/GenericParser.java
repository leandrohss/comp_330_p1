package com.leandro.twitterengine.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Leandro on 1/30/2016.
 */
public class GenericParser {


    public static List<String> parse(String text, String regex) {
        List result = new ArrayList();

        for (String word : text.split(" ")) {
            if (word.matches(regex))
                result.add(word);
        }

        return result;
    }

    public static List<List<String>> parse(final String text, final String... regex) {
        //TODO: may be interesting add concurrency here
        List<List<String>> results = new ArrayList<>();

        for (String r :  regex) {
            results.add(parse(text, r));
        }

        return results;
    }

}
