package com.leandro.twitterengine;

/**
 * A generic message
 *
 * Created by Leandro on 1/30/2016.
 */
public class Message {

    private String message;

    public Message(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
