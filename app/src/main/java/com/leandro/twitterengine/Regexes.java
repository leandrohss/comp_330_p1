package com.leandro.twitterengine;

/**
 * Created by Leandro on 2/1/2016.
 */
public final class Regexes {

    /** Regex for words with starts with # **/
    public static final String TOPICS = "^(#).*$";

    /** Regex for words with starts with @ **/
    public static final String MENTIONS = "^(@).*$";

    /** Regex for words with starts with http or https or ftp **/
    // Credits: Philip Daubmeier
    public static final String URIS =
            "\\b(((ht|f)tp(s?)\\:\\/\\/|~\\/|\\/)|www.)" +
            "(\\w+:\\w+@)?(([-\\w]+\\.)+(com|org|net|gov" +
            "|mil|biz|info|mobi|name|aero|jobs|museum" +
            "|travel|[a-z]{2}))(:[\\d]{1,5})?" +
            "(((\\/([-\\w~!$+|.,=]|%[a-f\\d]{2})+)+|\\/)+|\\?|#)?" +
            "((\\?([-\\w~!$+|.,*:]|%[a-f\\d{2}])+=?" +
            "([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)" +
            "(&(?:[-\\w~!$+|.,*:]|%[a-f\\d{2}])+=?" +
            "([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)*)*" +
            "(#([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)?\\b";


    //public static final String URIS = "^(http|https|ftp)://.*$";
}
