package com.leandro.twitterengine.util;

import com.leandro.twitterengine.Regexes;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;


public class GenericParserTest {

    private final String text =
            "@DonaldTrump will #never be the #president. Source: https://www.myself.com";

    @Test
    public void ParseTest() {
        List<String> result;

        // Test URLs
        result = GenericParser.parse(text, Regexes.URIS);
        assertEquals(result.size(), 1);
        assertEquals(result.get(0), "https://www.myself.com");

        // Test @
        result = GenericParser.parse(text, Regexes.MENTIONS);
        assertEquals(result.size(), 1);
        assertEquals(result.get(0), "@DonaldTrump");

        // Test #
        result = GenericParser.parse(text, Regexes.TOPICS);
        assertEquals(result.size(), 2);
        assertEquals(result.get(0), "#never");
        assertEquals(result.get(1), "#president.");
    }

    @Test
    public void ParseMultipleArgumentsTest() {
        List<List<String>> result;

        // Find: URLs, @, #
        result = GenericParser.parse(text, Regexes.URIS, Regexes.MENTIONS, Regexes.TOPICS);

        assertEquals(result.size(), 3);
        assertEquals(result.get(0).get(0), "https://www.myself.com");
        assertEquals(result.get(1).get(0), "@DonaldTrump");
        assertEquals(result.get(2).get(0), "#never");
        assertEquals(result.get(2).get(1), "#president.");

    }
}
