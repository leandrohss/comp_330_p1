package com.leandro.twitterengine;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class MessageTest {
    @Test
    public void ConstructorTest() {
        String sMsg = "test";

        Message msg = new Message(sMsg);
        assertEquals(msg.getMessage(), sMsg);
    }

    @Test
    public void GetAndSetMessageTest() {
        Message msg = new Message("first");
        msg.setMessage("second");

        assertEquals(msg.getMessage(), "second");
    }

}