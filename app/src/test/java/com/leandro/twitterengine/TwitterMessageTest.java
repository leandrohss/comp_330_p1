package com.leandro.twitterengine;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class TwitterMessageTest {

    private final String text =
            "@DonaldTrump will #never be the #president. Source: https://myself.com";

    @Test
    public void ConstructorTest() {
        TwitterMessage msg = new TwitterMessage(text);
        assertEquals(msg.getMessage(), text);
    }

    @Test
    public void GetAndSetMessageTest() {
        TwitterMessage msg = new TwitterMessage("first");
        msg.setMessage("second");

        assertEquals(msg.getMessage(), "second");
    }

    @Test
    public void GetMentionsTest() {
        TwitterMessage msg = new TwitterMessage(text);
        assertEquals(msg.getMentions().get(0), "@DonaldTrump");
    }

    @Test
    public void GetTopicsTest() {
        TwitterMessage msg = new TwitterMessage(text);
        assertEquals(msg.getTopics().get(0), "#never");
        assertEquals(msg.getTopics().get(1), "#president");
    }

    @Test
    public void GetURIsTest() {
        TwitterMessage msg = new TwitterMessage(text);
        assertEquals(msg.getUris().get(0), "https://myself.com");
    }

}